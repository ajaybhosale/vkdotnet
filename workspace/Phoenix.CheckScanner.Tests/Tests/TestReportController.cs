﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Phoenix.CheckScanner.Connectors;
using Phoenix.CheckScanner.Connectors.Contracts;
using Phoenix.CheckScanner.UI.Services.Controllers;
using System.Collections.Generic;

namespace Phoenix.CheckScanner.Tests.Tests
{
    [TestFixture]
    public class TestReportController
    {
        [Test]
        public void GetTransactionReport_when_Result_Null()
        {
            ReportController reportController = new ReportController();
            ReportFilters filters = null;
            reportController.ControllerContext = new ControllerContext();
            reportController.ControllerContext.HttpContext = new DefaultHttpContext();
            reportController.ControllerContext.HttpContext.Request.Headers["User-Agent"] = "1234";
            IEnumerable<TransactionModel> result = reportController.GetTransactionReport(filters);
            Assert.AreEqual(null, result);
        }
        //[Test]
        //public void GetTransactionReport()
        //{
        //    ReportController reportController = new ReportController();
        //    ReportFilters filters = new ReportFilters()
        //    {
        //        MerchantId = "d4d1fa30-18b2-4c08-84d2-96f0f1b73f6e"
        //    };

        //    reportController.ControllerContext = new ControllerContext();
        //    reportController.ControllerContext.HttpContext = new DefaultHttpContext();
        //    reportController.ControllerContext.HttpContext.Request.Headers["User-Agent"] = "1234";
        //    IEnumerable<TransactionModel> result = reportController.GetTransactionReport(filters);

        //    Assert.AreEqual(null, result);
        //}
    }
}
