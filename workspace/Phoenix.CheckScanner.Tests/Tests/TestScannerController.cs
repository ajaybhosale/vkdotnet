﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using Phoenix.CheckScanner.Connectors;
using Phoenix.CheckScanner.UI.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.CheckScanner.Tests
{
    [TestFixture]
    public class TestScannerController
    {
        private readonly Mock<IKeyVaultSecret> _keyVaultSecret = new Mock<IKeyVaultSecret>();

        [Test]
        public void Post_When_Scanner_Null()
        {
            ScannerController scannerController = new ScannerController(_keyVaultSecret.Object);
            Scanner scanner = null;
            scannerController.ControllerContext = new ControllerContext();
            Task<string> result = scannerController.Post(scanner);

            Assert.AreEqual(string.Empty, result.Result);
        }
        [Test]
        public void Post()
        {
            ScannerController scannerController = new ScannerController(_keyVaultSecret.Object);
            Scanner scanner = new Scanner()
            {
                Check_Status = "test",
                IP = "test",
                Merchant_Id = "d4d1fa30-18b2-4c08-84d2-96f0f1b73f6e"
            };
            string status = "success";
            scannerController.ControllerContext = new ControllerContext();
            scannerController.ControllerContext.HttpContext = new DefaultHttpContext();
            scannerController.ControllerContext.HttpContext.Request.Headers["User-Agent"] = "1234";
            Task<string> result = scannerController.Post(scanner);
            Assert.AreEqual(status, result.Result);
        }
        //[Test]
        //public void Delete()
        //{
        //    ScannerController scannerController = new ScannerController(_keyVaultSecret.Object);
        //    string id = "1";
        //    string status = "success";
        //    scannerController.ControllerContext = new ControllerContext();
        //    scannerController.ControllerContext.HttpContext = new DefaultHttpContext();
        //    scannerController.ControllerContext.HttpContext.Request.Headers["User-Agent"] = "1234";
        //    Task<string> result = scannerController.Delete(id);

        //    Assert.AreEqual(status, result.Result);
        //}
        //[Test]
        //public void Get_When_Result_Null()
        //{
        //    ScannerController scannerController = new ScannerController(_keyVaultSecret.Object);
        //    string id = "1";
        //    scannerController.ControllerContext = new ControllerContext();
        //    scannerController.ControllerContext.HttpContext = new DefaultHttpContext();
        //    scannerController.ControllerContext.HttpContext.Request.Headers["User-Agent"] = "1234";
        //    Task<Scanner> scanner = scannerController.Get(id);
        //    Assert.AreEqual(null, scanner.Result);
        //}
        //[Test]
        //public void GetAllScannerSetting_When_Result_Null()
        //{
        //    ScannerController scannerController = new ScannerController(_keyVaultSecret.Object);

        //    string merchantId = "1";
        //    scannerController.ControllerContext = new ControllerContext();
        //    scannerController.ControllerContext.HttpContext = new DefaultHttpContext();
        //    scannerController.ControllerContext.HttpContext.Request.Headers["User-Agent"] = "1234";
        //    IEnumerable<Scanner> scanners = scannerController.GetAllScannerSettings(merchantId);
        //    Assert.AreEqual(null, scanners);
        //}

        [Test]
        public void Post_When_Scanner_Error()
        {
            ScannerController objScannerController = new ScannerController(_keyVaultSecret.Object);
            Scanner scanner = new Scanner();
            scanner.IP = "10.4.1SSS";
            scanner.Port = "007";
            scanner.Serial_No = "12345560000";
            scanner.Source_Key = "09876543210987654321098765432109876543210987654321098765432109876543210987654321fake";
            objScannerController.ControllerContext = new ControllerContext();
            objScannerController.ControllerContext.HttpContext = new DefaultHttpContext();
            objScannerController.ControllerContext.HttpContext.Request.Headers["User-Agent"] = "1234";
            objScannerController.ModelState.AddModelError("fakeError", "fakeError-001");

            Task<string> result = objScannerController.Post(scanner);
            Assert.That(result.Result, Is.Not.Empty);
        }
    }
}
