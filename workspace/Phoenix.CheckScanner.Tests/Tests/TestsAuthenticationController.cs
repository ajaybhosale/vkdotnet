﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;
using Phoenix.CheckScanner.Connectors;
using Phoenix.CheckScanner.UI.Services;
using System.Threading.Tasks;

namespace Phoenix.CheckScanner.Tests.Tests
{

    [TestFixture]
   public class TestsAuthenticationController
    {
        private readonly Mock<IKeyVaultSecret> _keyVaultSecret = new Mock<IKeyVaultSecret>();
        private readonly Mock<IOptions<AzureCredentialsSettings>> _azureCredentialsSettings = new Mock<IOptions<AzureCredentialsSettings>>();
        private readonly Mock<IOptions<KeyVaultSettings>> _keyVaultSettings = new Mock<IOptions<KeyVaultSettings>>();

        //[Test]
        //public void Login_When_Result_Null()
        //{
        //    AzureCredentialsSettings setting = new AzureCredentialsSettings()
        //    {
        //        GrantType = "authorization_code",
        //        ClientId = "ff98dd8f-a643-4a28-ad63-064ba37fcd39",
        //        Scope = "https://phoenixiam.onmicrosoft.com/api/read openid profile",
        //        RedirectUri = "https%3A%2F%2Fwww.admin-ui.com%3A444%2Foauth",
        //        ClientSecret = "r24673G*29`B]6`m48[FY6)6",
        //        Url = "https://login.microsoftonline.com/phoenixiam.onmicrosoft.com/oauth2/v2.0/token?p=B2C_1_scanner-portal-local-sign-in"
        //    };

        //    _azureCredentialsSettings.Setup(ap => ap.Value).Returns(setting);
        //    AuthenticationController authenticationController = new AuthenticationController(_keyVaultSecret.Object, _keyVaultSettings.Object, _azureCredentialsSettings.Object);

        //    authenticationController.ControllerContext = new ControllerContext();
        //    authenticationController.ControllerContext.HttpContext = new DefaultHttpContext();
        //    authenticationController.ControllerContext.HttpContext.Request.Headers["User-Agent"] = "1234";

        //    string code = "Abc12";
        //    Task<string> scanner = authenticationController.Login(code);

        //    Assert.AreEqual("error", scanner.Result.ToLower());
        //}


        //[Test]
        //public void Login_When_Result_AuthenticationFailed()
        //{
        //    AzureCredentialsSettings setting = new AzureCredentialsSettings()
        //    {
        //        GrantType = "authorization_code",
        //        ClientId = "ff98dd8f-a643-4a28-ad63-064ba37fcd39",
        //        Scope = "https://phoenixiam.onmicrosoft.com/api/read openid profile",
        //        RedirectUri = "https%3A%2F%2Fwww.admin-ui.com%3A444%2Foauth",
        //        ClientSecret = "r24673G*29`B]6`m48[FY6)6",
        //        Url = "https://login.microsoftonline.com/phoenixiam.onmicrosoft.com/oauth2/v2.0/token?p=B2C_1_scanner-portal-local-sign-in"
        //    };
        //    _azureCredentialsSettings.Setup(ap => ap.Value).Returns(setting);
        //    AuthenticationController authenticationController = new AuthenticationController(_keyVaultSecret.Object, _keyVaultSettings.Object, _azureCredentialsSettings.Object);

        //    authenticationController.ControllerContext = new ControllerContext();
        //    authenticationController.ControllerContext.HttpContext = new DefaultHttpContext();
        //    authenticationController.ControllerContext.HttpContext.Request.Headers["User-Agent"] = "1234";

        //    string code = "Abc12";
        //    Task<string> scanner = authenticationController.Login(code);
        //    Assert.AreEqual("error", scanner.Result.ToLower());
        //}
    }
}
