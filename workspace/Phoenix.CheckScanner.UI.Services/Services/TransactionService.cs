﻿/*
 * VERICHECK INC CONFIDENTIAL
 * Vericheck Incorporated
 * All Rights Reserved.
 
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * category Serivces
 * package  TransactionService
 * author   VCI <info@vericheck.net>
 * license  Copyright 2018 VeriCheck | All Rights Reserved
 * version  GIT:$Id:
 * link     https://www.vericheck.com/doc
 */

using Phoenix.CheckScanner.Connectors;
using Phoenix.CheckScanner.Connectors.Azure;
using Phoenix.CheckScanner.Connectors.Azure.DocumentDB;
using Phoenix.CheckScanner.Connectors.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.CheckScanner.UI.Services
{
    /*
    * Transaction Service carries the request for transactions to pass it to the connector
    *
    * name     TransactionService
    * category Service
    * package  Transaction Service
    * author   VCI <info@vericheck.net>
    * license  Copyright 2018 VeriCheck | All Rights Reserved
    * version  GIT:
    * link     https://www.vericheck.com/docs/
    */
    /// <summary>
    /// Represents Transaction Service
    /// </summary>
    public class TransactionService
    {
        dynamic _transactionConnector;

        /*
         * Default constructor to iniliaze the transactionConnector instance
         *
         * name   TransactionService
         * access public
         * author VCI <info@vericheck.net>
         *
         * return void
         */
        public TransactionService(Object transactionConnector)
        {
            _transactionConnector = transactionConnector;
        }

        /*
      * Function creates the USePay transaction
      *
      * param documentDBService is used to access the services of DocumentDB, transactionList is the list of Transactions which would be created in DB, 
      * transactionAPISettings contains the Source key and Pin to get the token, Request object is used during activity and error logging.
      *
      * name   CreateUSePayTransactions
      * access public
      * author VCI <info@vericheck.net>
      *
      * return IEnumerable TransactionModel
     */
        public async Task<IEnumerable<TransactionModel>> CreateUSePayTransactions(DocumentDBConnector<TransactionModel> documentDbService, List<TransactionModel> transactionList, TransactionAPISettings transactionAPISettings, RequestModel request)
        {
            return  await _transactionConnector.CreateTransactions(documentDbService, transactionList, transactionAPISettings, request);
        }

        /*
         * Function creates the Legacy transaction
         *
         * param documentDBService is used to access the services of DocumentDB, transactionList is the list of Transactions which would be created in DB, 
         * transactionAPISettings contains the Source key and Pin to get the token, Request object is used during activity and error logging.
         *
         * name   CreateLegacyTransactions
         * access public
         * author VCI <info@vericheck.net>
         *
         * return IEnumerable TransactionModel
        */
        public async Task<IEnumerable<TransactionModel>> CreateLegacyTransactions(DocumentDBConnector<TransactionModel> documentDbService, List<TransactionModel> transactionList, TransactionAPISettings transactionAPISettings, RequestModel request)
        {
            return await _transactionConnector.CreateTransactions(documentDbService, transactionList, transactionAPISettings, request);
        }

        /*
         * Function gets the Transaction details by Id
         *
         * param Obj documentDBService is used to access the services of DocumentDB, based on the type, merchantId and transaction group Id the list of Transaction details are retrived from DB, 
         * Request object is used during activity and error logging.
         *
         * name   GetTransactionDetailsbyId
         * access public
         * author VCI <info@vericheck.net>
         *
         * return IEnumerable TransactionModel
        */
        public IEnumerable<TransactionModel> GetTransactionDetailsbyId(DocumentDBConnector<TransactionModel> documentDbService, string merchantId, string type, RequestModel request,string transactionGroupId)
        {
            return _transactionConnector.GetTransactionDetails(documentDbService, merchantId, type, request, transactionGroupId);
        }

        /*
        * Function creates the Phoenix transaction
        *
        * 
        *
        * name   CreatePhoenixTransactions
        * access public
        * author VCI <info@vericheck.net>
        *
        * return void
       */
        public void CreatePhoenixTransactions()
        {
            //Future implementation
        }
    }
}
