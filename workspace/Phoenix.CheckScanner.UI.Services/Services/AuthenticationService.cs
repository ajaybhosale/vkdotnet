﻿/*
 * VERICHECK INC CONFIDENTIAL
 * Vericheck Incorporated
 * All Rights Reserved.
 
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * category Serivces
 * package  AuthenticationService
 * author   VCI <info@vericheck.net>
 * license  Copyright 2018 VeriCheck | All Rights Reserved
 * version  GIT:$Id:
 * link     https://www.vericheck.com/doc
 */

using Phoenix.CheckScanner.Connectors;
using System.Threading.Tasks;

namespace Phoenix.CheckScanner.UI.Services
{
    /*
    * This service handles the request received from the Authentication Controller and decides to forward the request further 
    *
    * name     AuthenticationService
    * category Service
    * package  Authentication Service
    * author   VCI <info@vericheck.net>
    * license  Copyright 2018 VeriCheck | All Rights Reserved
    * version  GIT:
    * link     https://www.vericheck.com/docs/
    */
    /// <summary>
    /// Represents Authentication Service
    /// </summary>
    public class AuthenticationService
    {
        ActiveDirectoryConnector _activeDirectoryConnector;

        /*
       *  Default constructor to iniliaze the ActiveDirectoryConnector instance
       *
       * param code
       *
       * name   AuthenticationService
       * access public
       * author VCI <info@vericheck.net>
       *
       * return void
  */
        public AuthenticationService(ActiveDirectoryConnector ActiveDirectoryConnector )
        {
            _activeDirectoryConnector = ActiveDirectoryConnector;
        }

        /*
         * Function validates the Azure credentials.s
         *
         * param code
         *
         * name   ValidateAzureCredentials
         * access public
         * author VCI <info@vericheck.net>
         *
         * return string
    */
        public string ValidateAzureCredentials(AzureCredentialsSettings azureCredetials)
        {
            // get the user Authorize with Azure AD
            return _activeDirectoryConnector.ValidateAzureCredentials(azureCredetials);
        }
        public async Task<string> GetClientToken(AzureCredentialsSettings _azureCredentialsSettings)
        {
            return await _activeDirectoryConnector.GetClientToken(_azureCredentialsSettings);
        }


    }
}
