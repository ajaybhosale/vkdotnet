﻿/*
 * VERICHECK INC CONFIDENTIAL
 * Vericheck Incorporated
 * All Rights Reserved.
 
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * category Serivces
 * package  ScannerService
 * author   VCI <info@vericheck.net>
 * license  Copyright 2018 VeriCheck | All Rights Reserved
 * version  GIT:$Id:
 * link     https://www.vericheck.com/doc
 */

using Phoenix.CheckScanner.Connectors;
using Phoenix.CheckScanner.Connectors.Azure;
using Phoenix.CheckScanner.Connectors.Azure.DocumentDB;
using Phoenix.CheckScanner.Connectors.Contracts;
using Phoenix.CheckScanner.Connectors.Scanners;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.CheckScanner.UI.Services.Services
{
    /*
     * Scanner Service carries the Scanner related requests received from the controller
     *
     * name     ScannerService
     * category Service
     * package  Scanner Service
     * author   VCI <info@vericheck.net>
     * license  Copyright 2018 VeriCheck | All Rights Reserved
     * version  GIT:
     * link     https://www.vericheck.com/docs/
     */
    /// <summary>
    /// Represents Scanner Service
    /// </summary>
    public class ScannerService
    {
        TS240Connector _TS240Connector;

        /*
         * Default constructor to iniliaze the TS240Connector instance
         *
         * name   ScannerService
         * access public
         * author VCI <info@vericheck.net>
         *
         * return void
         */
        public ScannerService(TS240Connector TS240Connector)
        {
            _TS240Connector = TS240Connector;
        }

        /*
       * Function creates or updates(if exists) the scanner settings in DB.
       *
       * param documentDBService is used to access the services of DocumentDB, scanner is the instance of Scanner class which would be saved, 
       * Request object is used during activity and error logging.
       *
       * name   SaveScannerSettings
       * access public
       * author VCI <info@vericheck.net>
       *
       * return string
      */
        public async Task<string> SaveScannerSettings(DocumentDBConnector<Scanner> documentDbService, Scanner scanner, RequestModel request)
        {
            return await _TS240Connector.SaveScannerSettings(documentDbService, scanner, request);
        }

        /*
          * Function gets the details of Scanner based on the passed paramater.
          *
          * param Obj documentDBService is used to access the services of DocumentDB, based on the id the Scanner details are retrived from DB, 
          * Request object is used during activity and error logging.
          *
          * name   GetScannerSettings
          * access public
          * author VCI <info@vericheck.net>
          *
          * return Scanner 
         */
        public Scanner GetScannerSettings(DocumentDBConnector<Scanner> documentDbService, string id)
        {
            return _TS240Connector.GetScannerSettings(documentDbService, id);
        }

        /*
         * Function gets the list of all Scanner based on the paramaters passed.
         *
         * param Obj documentDBService is used to access the services of DocumentDB, based on the type and merchantId the list of Scanner settings are retrived from DB, 
         * Request object is used during activity and error logging.
         *
         * name   GetAllScannerSettings
         * access public
         * author VCI <info@vericheck.net>
         *
         * return List of Scanner 
        */
        public IEnumerable<Scanner> GetAllScannerSettings(DocumentDBConnector<Scanner> documentDbService, string type, string merchantId)
        {
            return _TS240Connector.GetAllScannerSettings(documentDbService, type, merchantId);
        }

        /*
       * Function deletes the Scanner from DB based on the passed paramater id.
       *
       * param Obj documentDBService is used to access the services of DocumentDB, id is the document id of Scanner, 
       * Request object is used during activity and error logging.
       *
       * name   DeleteScannerSetting
       * access public
       * author VCI <info@vericheck.net>
       *
       * return string 
      */
        public async Task<string> DeleteScannerSetting(DocumentDBConnector<Scanner> documentDbService, string id, RequestModel request)
        {
            return await _TS240Connector.DeleteScannerSetting(documentDbService, id, request);
        }
    }
}
