﻿/*
 * VERICHECK INC CONFIDENTIAL
 * Vericheck Incorporated
 * All Rights Reserved.
 
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * category Serivces
 * package  CheckService
 * author   VCI <info@vericheck.net>
 * license  Copyright 2018 VeriCheck | All Rights Reserved
 * version  GIT:$Id:
 * link     https://www.vericheck.com/doc
 */

using Phoenix.CheckScanner.Connectors;
using Phoenix.CheckScanner.Connectors.Azure;
using Phoenix.CheckScanner.Connectors.Azure.DocumentDB;
using Phoenix.CheckScanner.Connectors.Check;
using Phoenix.CheckScanner.Connectors.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.CheckScanner.UI.Services.Services
{
    /*
     * Check service works as a mediator between the controller and the connetor
     *
     * name     CheckService
     * category Service
     * package  Check Service
     * author   VCI <info@vericheck.net>
     * license  Copyright 2018 VeriCheck | All Rights Reserved
     * version  GIT:
     * link     https://www.vericheck.com/docs/
     */
    /// <summary>
    /// Represents Check scanning
    /// </summary>
    public class CheckService
    {
        ChecksConnector _checkConnector;

        /*
        * Default constructor to iniliaze the checkconnector instance
        *
        * name   CheckService
        * access public
        * author VCI <info@vericheck.net>
        *
        * return void
        */
        public CheckService(ChecksConnector checkConnector)
        {
            _checkConnector = checkConnector;
        }

        /*
        * Function saves the transaction details such as Batch Id, Type and check status
        *
        * param documentDBService is used to access the services of DocumentDB, transactions contains the list of transactions , 
        * Request object is used during activity and error logging.
        *
        * name   SaveCheckDetails
        * access public
        * author VCI <info@vericheck.net>
        *
        * return string
       */
        public async Task<List<TransactionModel>> SaveCheckDetails(DocumentDBConnector<TransactionModel> documentDbService, List<TransactionModel> transactions,RequestModel request)
        {
             return await _checkConnector.SaveCheckDetails(documentDbService, transactions, request);
        }

        /*
         * Function verifies the routes and accounts of transactions.
         *
         * param Obj verificationAPISettings is contains the credentials , transactions contains the list of transactions , 
         * Request object is used during activity and error logging.
         *
         * name   VerifyRouteAccount
         * access public
         * author VCI <info@vericheck.net>
         *
         * return List of TransactionModel 
        */
        public async Task<List<TransactionModel>>  VerifyRouteAccount(List<TransactionModel> transactions,RequestModel request)
        {
            return await  _checkConnector.VerifyRouteAccount(transactions,request);
        }

      /*
       * Function deletes the Scanned check from DB based on the passed paramater id.
       *
       * param Obj documentDBService is used to access the services of DocumentDB, id is the document id of check, 
       * Request object is used during activity and error logging.
       *
       * name   DeleteCheckDetails
       * access public
       * author VCI <info@vericheck.net>
       *
       * return bool 
      */
        public async Task<bool> DeleteCheckDetails(IDocumentDbService<TransactionModel> documentDbService,string id,RequestModel request)
        {
            return await _checkConnector.DeleteCheckDetails(documentDbService, id,request);
        }

        /*
       * Function gets the details of Scanned check based on the passed paramater.
       *
       * param Obj documentDBService is used to access the services of DocumentDB, based on the merchantID and type the Scanned checks are retrived from DB, 
       * Request object is used during activity and error logging.
       *
       * name   GetScannedChecks
       * access public
       * author VCI <info@vericheck.net>
       *
       * return TransactionModel 
      */
        public IEnumerable<TransactionModel> GetScannedChecks(IDocumentDbService<TransactionModel> documentDbService, string merchantId,string type,RequestModel request)
        {
            return _checkConnector.GetScannedChecks(documentDbService, merchantId,type,request);
        }


        /*
        * Function updates the transaction which is passed as parameter.
        *
        * param Obj documentDBService is used to access the services of DocumentDB, transaction is the instance of TransactionModel which would edited, 
        * Request object is used during activity and error logging.
        *
        * name   UpdateCheckDetails
        * access public
        * author VCI <info@vericheck.net>
        *
        * return string 
       */
        public async Task<string> UpdateCheckDetails(IDocumentDbService<TransactionModel> documentDbService, TransactionModel transaction,RequestModel request)
        {
            return await _checkConnector.UpdateCheckDetails(documentDbService, transaction, request);
        }

        /*
        * Function gets all the transactions having same merchant Id and type passed as paramater.
        *
        * param Obj documentDBService is used to access the services of DocumentDB, based on merchantID and type the transactions are retrived from DB, 
        * Request object is used during activity and error logging.
        *
        * name   GetChecksForReview
        * access public
        * author VCI <info@vericheck.net>
        *
        * return TransactionModel 
       */
        public IEnumerable<TransactionModel> GetChecksForReview(IDocumentDbService<TransactionModel> documentDbService, string merchantId, string type, RequestModel request)
        {
            return _checkConnector.GetChecksForReview(documentDbService, merchantId, type, request);
        }

        /*
         * Function updates all the transactions having same merchant Id and type passed as paramater.
         *
         * param Obj documentDBService is used to access the services of DocumentDB, based on merchantID and type the transactions are retrived from DB, 
         * Request object is used during activity and error logging.
         *
         * name   UpdateChecksForReview
         * access public
         * author VCI <info@vericheck.net>
         *
         * return Task of string
        */
        public async Task<string> UpdateChecksForReview(IDocumentDbService<TransactionModel> documentDbService, string merchantId, string type, RequestModel request)
        {
            return await _checkConnector.UpdateChecksForReview(documentDbService, merchantId, type, request);
        }
        public IEnumerable<TransactionModel> GetUnsavedChecks(IDocumentDbService<TransactionModel> documentDbService, string merchantId, string type, RequestModel request)
        {
            return _checkConnector.GetUnsavedChecks(documentDbService, merchantId, type, request);
        }
    }
}
