﻿
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Phoenix.CheckScanner.UI.Services.Auth
{
    public class Authorization11
    {
        public Authorization11() { }
        public bool AuthorizeUser(string clientToken)
        {
            bool isValid = false;
            try
            {
                if (clientToken != null)
                {
                    
                    string[] clientAccessTokenArray = clientToken.Split(',');
                    if (clientAccessTokenArray != null)
                    {
                        JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();

                       
                        var handler = new JwtSecurityTokenHandler();
                       
                        var clientTokenS = handler.ReadJwtToken(clientAccessTokenArray[0].Split('"')[3]).Claims;

                        var issuer = clientTokenS.Where(a => a.Type == "iss").FirstOrDefault();
                        

                        //validate token expiry
                        var expiry = clientTokenS.Where(a => a.Type == "exp").FirstOrDefault();
                        isValid = IsExpired(Convert.ToDouble(expiry));
                        if (isValid)
                            return isValid;
                        //validate user Id
                        var userId= clientTokenS.Where(a => a.Type == "oid").FirstOrDefault();
                        var clientUserId = clientTokenS.Where(a => a.Type == "oid").FirstOrDefault();
                        if (userId == clientUserId)
                            isValid = true;
                        else
                           return isValid = false;

                        //validate Signature
                        var signature = handler.ReadJwtToken(clientAccessTokenArray[0].Split('"')[3]).RawSignature;
                        var clientSignature = handler.ReadJwtToken(clientAccessTokenArray[0].Split('"')[3]).RawSignature;
                        if (signature.Equals(clientSignature))
                            isValid = true;
                        else
                          return isValid = false;
                        var audience = clientTokenS.Where(a => a.Type == "aud").FirstOrDefault();
                        TokenValidationParameters validationParameters = new TokenValidationParameters
                        {
                            // We accept both the App Id URI and the AppId of this service application
                            ValidAudiences = new[] { audience.ToString(), "ff98dd8f-a643-4a28-ad63-064ba37fcd39" },

                            // Supports both the Azure AD V1 and V2 endpoint
                            ValidIssuers = new[] { issuer.ToString(), $"{issuer.ToString()}/v2.0" },
                           
                        };
                        SecurityToken validatedToken = new JwtSecurityToken();
                        ClaimsPrincipal claimsPrincipal = tokenHandler.ValidateToken(clientToken, validationParameters, out validatedToken);

                    }
                }
            }
            catch (Exception ex)
            {

            }
            return isValid;
        }
        private bool IsExpired(double createdTimeStamp)
        {
            double today = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmssffff"));
            if (today > createdTimeStamp)
                return true;
            else
                return false;

        }
    }
}
