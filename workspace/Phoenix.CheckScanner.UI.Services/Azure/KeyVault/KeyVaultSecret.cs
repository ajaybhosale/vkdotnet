﻿/*
 * VERICHECK INC CONFIDENTIAL
 * Vericheck Incorporated
 * All Rights Reserved.
 
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * category Secret key vault
 * package  KeyVaultSecret
 * author   VCI <info@vericheck.net>
 * license  Copyright 2018 VeriCheck | All Rights Reserved
 * version  GIT:$Id:
 * link     https://www.vericheck.com/doc
 */

using Microsoft.Azure.KeyVault;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Phoenix.CheckScanner.Connectors;
using Phoenix.CheckScanner.Connectors.Azure.ServiceBus;
using Phoenix.CheckScanner.Connectors.Contracts;
using System;
using System.Threading.Tasks;

namespace Phoenix.CheckScanner.UI.Services
{
    public class KeyVaultSecret : IKeyVaultSecret
    {
        private readonly KeyVaultSettings _keyVaultSettings;
        private static string ClientId;
        private static string SecureClientSecret;
        private static string keyVaultUri;

        /*
         * Parameterized constructor inject the KeyVaultSettings class
         * To initialize the various properties such as keyVaultSettings, Client Id, SecureClientSecret and keyVaultUri
         *
         * name   KeyVaultSecret
         * access public
         * author VCI <info@vericheck.net>
         *
         * return void
        */
        public KeyVaultSecret(IOptions<KeyVaultSettings> keyVaultSettings)
        {
            _keyVaultSettings = keyVaultSettings.Value;
            ClientId = _keyVaultSettings.ClientId;
            SecureClientSecret = _keyVaultSettings.SecretKey;
            keyVaultUri = _keyVaultSettings.KeyVaultUri;
        }

        #region -- Public Methods --
        /*
         * Parameterized method gets the secret async instance 
         * 
         * param keyIdentifier and requestModel
         *
         * name   GetSecretAsync
         * access public
         * author VCI <info@vericheck.net>
         *
         * return string
        */
        /// <summary>
        /// Gets the secret from Axure key vault
        /// </summary>
        /// <param name="keyIdentifier">Secret key identifier</param>
        /// <returns>String secret</returns>
        public async Task<string> GetSecretAsync(string keyIdentifier,RequestModel requestModel)
        {
            try
            {
                var keyVaultClient = new KeyVaultClient(AuthenticateVaultAsync);
                var result = await keyVaultClient.GetSecretAsync(string.Concat(keyVaultUri, "secrets/", keyIdentifier));

                return result.Value.ToString();
            }
            catch (Exception e)
            {
                ServiceBusConnector serviceBus = new ServiceBusConnector();
                serviceBus.LogError(e, requestModel);
                return null;
            }
        }
        /// <summary>
        /// Sets the key in Azure key vault
        /// </summary>
        /// <param name="secretName">Secret key/name/identifier</param>
        /// <param name="secretValue">Secret value</param>
        /// <returns></returns>
        public async Task<string> SetSecretAsync(string secretName, string secretValue)
        {
            try
            {
                var keyVaultClient = new KeyVaultClient(AuthenticateVaultAsync);
                var result = await keyVaultClient.SetSecretAsync(keyVaultUri, secretName, secretValue);

                return result.SecretIdentifier.Name;
            }
            catch (Exception e)
            {
                return null;
            }

        }
        #endregion
        #region -- Private Methods --
        /*
        * Function authenticates the Vault async
        * 
        * param authority,resource and scope
        *
        * name   AuthenticateVaultAsync
        * access public
        * author VCI <info@vericheck.net>
        *
        * return string
       */
        private async Task<string> AuthenticateVaultAsync(string authority, string resource, string scope)
        {
            var clientCredentials = new ClientCredential(ClientId, SecureClientSecret);
            var authenticationContext = new AuthenticationContext(authority);
            var result = await authenticationContext.AcquireTokenAsync(resource, clientCredentials);
            return result.AccessToken;
        }
        #endregion
    }
}
