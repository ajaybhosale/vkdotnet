﻿using Phoenix.CheckScanner.Connectors.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.CheckScanner.UI.Services
{
    public interface IKeyVaultSecret
    {
        Task<string> GetSecretAsync(string keyIdentifier, RequestModel request);
        Task<string> SetSecretAsync(string secretName, string secretValue);
    }
}
