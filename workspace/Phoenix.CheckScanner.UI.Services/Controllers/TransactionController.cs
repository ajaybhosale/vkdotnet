﻿/*
 * VERICHECK INC CONFIDENTIAL
 * Vericheck Incorporated
 * All Rights Reserved.
 
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * category Controller
 * package  TransactionController
 * author   VCI <info@vericheck.net>
 * license  Copyright 2018 VeriCheck | All Rights Reserved
 * version  GIT:$Id:
 * link     https://www.vericheck.com/doc
 */

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Phoenix.CheckScanner.Connectors;
using Phoenix.CheckScanner.Connectors.Azure;
using Phoenix.CheckScanner.Connectors.Azure.DocumentDB;
using Phoenix.CheckScanner.Connectors.Contracts;
using Phoenix.CheckScanner.UI.Services.Azure;
using Phoenix.CheckScanner.UI.Services.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.CheckScanner.UI.Services
{
    /*
   * Transaction controller Handles the transaction requests for the user
   *
   * name     TransactionController
   * category Controller
   * package  Transaction Controller
   * author   VCI <info@vericheck.net>
   * license  Copyright 2018 VeriCheck | All Rights Reserved
   * version  GIT:
   * link     https://www.vericheck.com/docs/
   */
    [Produces("application/json")]
    [Route("transaction")]
    [EnableCors("ScannerCors")]
    [AuthorizeToken]
    public class TransactionController : Controller
    {
        private readonly TransactionAPISettings _transactionAPISettings;

        /*
         * Parameterized constructor to iniliaze the transaction API settings
         *
         * param transactionAPISettings
         * 
         * name   TransactionController
         * access public
         * author VCI <info@vericheck.net>
         *
         * return void
         */
        public TransactionController(IOptions<TransactionAPISettings> transactionAPISettings)
        {
            _transactionAPISettings = transactionAPISettings.Value;
        }


        /*
          * Function prepares the transaction from transactionlist 
          *
          * param list of transactions,
          *
          * name   MakeTransaction
          * access public
          * author VCI <info@vericheck.net>
          *
          * return IEnumerable of TransactionModel
         */
        /// <summary>
        /// Create transaction 
        /// </summary>
        /// <param name="transactionList"></param>
        /// <returns></returns>
        [HttpPost("makeTransaction")]
        public async Task<IEnumerable<TransactionModel>> MakeTransaction([FromBody]List<TransactionModel> transactionList)
        {
            ActivityLogHelper activityLogHelper = new ActivityLogHelper();
            activityLogHelper.LogActivity(Request, "Post", "Transaction", "Making transactions");

            //Prepared to RequestModel object to provide the details while logging the error.
            RequestModel request = activityLogHelper.PrepareRequestModel(Request.Headers["User-Agent"].ToString(), Request.Headers["X-Original-For"].ToString(), Request.Headers["Host"].ToString(), "Post", "Transaction", "Making transactions");

            IEnumerable<TransactionModel> updatedTransactionList =null;
            var _documentDbService = DocumentDbServicesFactory<TransactionModel>.GetDocumentDbInstance();
            if (transactionList != null && transactionList.Any())
            {
                if (transactionList.FirstOrDefault().Transaction_Gateway == GatewayTypes.VCI)
                {
                    TransactionService transactionService = new TransactionService(ConnectorServicesFactory.GetLegacyInstance());

                    BlobsHelper blobHelper = new BlobsHelper(transactionList.FirstOrDefault().Merchant_Id, request);
                    transactionList = await blobHelper.GetImageContentFromAzureByBlobId(transactionList, request);

                    updatedTransactionList = await transactionService.CreateLegacyTransactions(_documentDbService, transactionList, _transactionAPISettings, request);

                }
                else if (transactionList.FirstOrDefault().Transaction_Gateway == GatewayTypes.USA)
                {
                    TransactionService transactionService = new TransactionService(ConnectorServicesFactory.GetUSAePayInstance());
                    BlobsHelper blobHelper = new BlobsHelper(transactionList.FirstOrDefault().Merchant_Id, request);
                    transactionList = await blobHelper.GetImageContentFromAzureByBlobId(transactionList, request);

                    updatedTransactionList = await transactionService.CreateUSePayTransactions(_documentDbService, transactionList, _transactionAPISettings, request);
                }
                else if (transactionList.FirstOrDefault().Transaction_Gateway == GatewayTypes.PHN)
                {
                    //Future implementation
                }
                //Update transaction id in image metadata
                if (updatedTransactionList != null && updatedTransactionList.Any())
                {
                    BlobsHelper blobHelper = new BlobsHelper(updatedTransactionList.FirstOrDefault().Merchant_Id, request);
                    await blobHelper.UpdateMetaData(updatedTransactionList, request);
                }
            }
            return updatedTransactionList;
        }

        /*
         * Function gets the transaction details based on the parameters passed.
         *
         * param merchant Id and Transaction group id.
         *
         * name   GetTransactionDetailsbyId
         * access public
         * author VCI <info@vericheck.net>
         *
         * return IEnumerable of TransactionModel
        */
        /// <summary>
        /// Get transaction details by group Id
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="transactionGroupId"></param>
        /// <returns></returns>
        [HttpGet("getTransactionDetails")]
        public IEnumerable<TransactionModel> GetTransactionDetailsbyId(string merchantId, string transactionGroupId)
        {
            ActivityLogHelper activityLogHelper = new ActivityLogHelper();
            TransactionService transactionService = new TransactionService(ConnectorServicesFactory.GetLegacyInstance());
            var _documentDbService = DocumentDbServicesFactory<TransactionModel>.GetDocumentDbInstance();
            //Prepared to RequestModel object to provide the details while logging the error.
            RequestModel request = activityLogHelper.PrepareRequestModel(Request.Headers["User-Agent"].ToString(), Request.Headers["X-Original-For"].ToString(), Request.Headers["Host"].ToString(), "Get", "Transaction", "Get transaction details by Id");

            IEnumerable<TransactionModel> transactionList = transactionService.GetTransactionDetailsbyId(_documentDbService, merchantId, VCSConstants.Transaction, request, transactionGroupId);
            return transactionList;
        }


        [HttpGet("refreshtokens")]
        public string Refreshtokens()
        {
            return "Test..Success";
            //Prepared to RequestModel object to provide the details while logging the error.
           // RequestModel request = activityLogHelper.PrepareRequestModel(Request.Headers["User-Agent"].ToString(), Request.Headers["X-Original-For"].ToString(), Request.Headers["Host"].ToString(), "Post", "Transaction", "Making transactions");
            

        }

    }
}
