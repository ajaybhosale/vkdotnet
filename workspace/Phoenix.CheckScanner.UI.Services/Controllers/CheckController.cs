﻿/*
 * VERICHECK INC CONFIDENTIAL
 * Vericheck Incorporated
 * All Rights Reserved.
 
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * category Controller
 * package  CheckController
 * author   VCI <info@vericheck.net>
 * license  Copyright 2018 VeriCheck | All Rights Reserved
 * version  GIT:$Id:
 * link     https://www.vericheck.com/doc
 */

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Phoenix.CheckScanner.Connectors;
using Phoenix.CheckScanner.Connectors.Azure.DocumentDB;
using Phoenix.CheckScanner.Connectors.Azure.ServiceBus;
using Phoenix.CheckScanner.Connectors.Common.Constants;
using Phoenix.CheckScanner.Connectors.Contracts;
using Phoenix.CheckScanner.UI.Services.Auth;
using Phoenix.CheckScanner.UI.Services.Azure;
using Phoenix.CheckScanner.UI.Services.Helpers;
using Phoenix.CheckScanner.UI.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.CheckScanner.UI.Services
{
    /*
    * Works as a receiver for the Check related requests like GetCheckDetails,SaveCheckDetails for creating the transactions 
    *
    * name     CheckController
    * category Controller
    * package  Check Controller
    * author   VCI <info@vericheck.net>
    * license  Copyright 2018 VeriCheck | All Rights Reserved
    * version  GIT:
    * link     https://www.vericheck.com/docs/
    */
    [Produces("application/json")]
    [Route("check")]
    [EnableCors("ScannerCors")]
    [AuthorizeToken]
    public class CheckController : Controller
    {
        /*
       * Function saves check details
       *
       * param list of transactions
       *
       * name   Post
       * access public
       * author VCI <info@vericheck.net>
       *
       * return List of transaction
      */
        /// <summary>
        /// Save Scanned check details
        /// </summary>
        /// <param name="Merchant"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IEnumerable<TransactionModel>> Post([FromBody]List<TransactionModel> transactions)
        {
            if (transactions != null && transactions.Any())
            {
                ActivityLogHelper activityLogHelper = new ActivityLogHelper();
                //Validate Check Details
                CheckService checkService = new CheckService(ConnectorServicesFactory.GetChecksInstance());
                var _documentDbService = DocumentDbServicesFactory<TransactionModel>.GetDocumentDbInstance();
                //Prepared to RequestModel object to provide the details while logging the error.
                RequestModel request = activityLogHelper.PrepareRequestModel(Request.Headers["User-Agent"].ToString(), Request.Headers["X-Original-For"].ToString(), Request.Headers["Host"].ToString(), "Post", "Check", "Posintg scanned check details");

                transactions = await checkService.VerifyRouteAccount(transactions,request);

                //Upload check images on blob storage
                BlobsHelper blobHelper = new BlobsHelper(transactions.FirstOrDefault().Merchant_Id, request);
                transactions = await blobHelper.UploadImageToBlob(transactions, request);
                if (transactions != null)
                {
                    //Save scanned check details in scanner db
                   transactions=await checkService.SaveCheckDetails(_documentDbService, transactions, request);
                }
                else
                {
                    return null;
                }
            }
            return transactions;
        }

        /*
         * Function updates check details
         *
         * param list of transactions
         *
         * name   Post
         * access public
         * author VCI <info@vericheck.net>
         *
         * return List of transaction
        */
        /// <summary>
        /// Update check details
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<TransactionModel> Put([FromBody]TransactionModel transaction)
        {
            if (transaction != null)
            {
                ActivityLogHelper activityLogHelper = new ActivityLogHelper();
                CheckService checkService = new CheckService(ConnectorServicesFactory.GetChecksInstance());
                var _documentDbService = DocumentDbServicesFactory<TransactionModel>.GetDocumentDbInstance();
                List<TransactionModel> transactionList = new List<TransactionModel>();
                //Prepared to RequestModel object to provide the details while logging the error.
                RequestModel request = activityLogHelper.PrepareRequestModel(Request.Headers["User-Agent"].ToString(), Request.Headers["X-Original-For"].ToString(), Request.Headers["Host"].ToString(), "Put", "Check", "Updating scanned check");

                transactionList.Add(transaction);
                transactionList = await checkService.VerifyRouteAccount(transactionList, request);
                if (transactionList != null && transactionList.Any() && transactionList.FirstOrDefault().IsValid)
                {
                    transaction = transactionList.FirstOrDefault();

                    transaction.Check_Status = CheckStatus.Edit;
                    string frontImage = transaction.Front_Image;
                    string backImage = transaction.Back_Image;

                    transaction.Front_Image = string.Empty;
                    transaction.Back_Image = string.Empty;

                    await checkService.UpdateCheckDetails(_documentDbService, transaction, request);
                    transaction.Front_Image = frontImage;
                    transaction.Back_Image = backImage;
                }
                return transactionList.FirstOrDefault();
            }
            else
            {
                return null;
            }

        }

        /*
        * Function updates check details
        *
        * param list of transactions
        *
        * name   updateReviewDetails
        * access public
        * author VCI <info@vericheck.net>
        *
        * return TransactionModel
       */
        /// <summary>
        /// Update check details from review page
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        [HttpPost("updateReviewDetails")]
        public async Task<TransactionModel> UpdateReviewDetails([FromBody]TransactionModel transaction)
        {
            if (transaction != null)
            {
                ActivityLogHelper activityLogHelper = new ActivityLogHelper();
                CheckService checkService = new CheckService(ConnectorServicesFactory.GetChecksInstance());
                var _documentDbService = DocumentDbServicesFactory<TransactionModel>.GetDocumentDbInstance();
                //Prepared to RequestModel object to provide the details while logging the error.
                RequestModel request = activityLogHelper.PrepareRequestModel(Request.Headers["User-Agent"].ToString(), Request.Headers["X-Original-For"].ToString(), Request.Headers["Host"].ToString(), "Post", "Check", "Updating review details");
                
                List<TransactionModel> transactionList = new List<TransactionModel>();
                transactionList.Add(transaction);
                transactionList = await checkService.VerifyRouteAccount(transactionList, request);
                if (transactionList != null && transactionList.Count > 0 && transactionList.FirstOrDefault().IsValid)
                {
                    transaction.Front_Image = string.Empty;
                    transaction.Back_Image = string.Empty;
                    await checkService.UpdateCheckDetails(_documentDbService, transactionList.FirstOrDefault(), request);
                }

                return transactionList.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        /*
        * Function gets the check details based on parameters passed.
        *
        * param id as document Id
        *
        * name   GetCheck
        * access public
        * author VCI <info@vericheck.net>
        *
        * return IEnumerable of TransactionModel
       */
        /// <summary>
        /// Get all merchant wise check details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        
        [HttpGet("getCheck")]
        public async Task<IEnumerable<TransactionModel>> GetCheck(string id)
        {
            //Authorization11 authorize = new Authorization11();
            //await authorize.auth(authorization);
            IEnumerable<TransactionModel> transactionList = null;
            if (!string.IsNullOrEmpty(id))
            {
                ActivityLogHelper activityLogHelper = new ActivityLogHelper();
                CheckService checkService = new CheckService(ConnectorServicesFactory.GetChecksInstance());
                var _documentDbService = DocumentDbServicesFactory<TransactionModel>.GetDocumentDbInstance();
                //Prepared to RequestModel object to provide the details while logging the error.
                RequestModel request = activityLogHelper.PrepareRequestModel(Request.Headers["User-Agent"].ToString(), Request.Headers["X-Original-For"].ToString(), Request.Headers["Host"].ToString(), "Get", "Check", "Gets check detail");
               

               transactionList = checkService.GetScannedChecks(_documentDbService, id, VCSConstants.Transaction, request);
                if (transactionList != null && transactionList.Any())
                {
                    BlobsHelper blobHelper = new BlobsHelper(transactionList.FirstOrDefault().Merchant_Id, request);
                    transactionList = await blobHelper.GetImageContentFromAzureByBlobId(transactionList, request);
                }
            }
            return transactionList;
        }

        /*
        * Function gets the check details based on parameters passed.
        *
        * param id as document Id
        *
        * name   GetChecksForReview
        * access public
        * author VCI <info@vericheck.net>
        *
        * return IEnumerable of TransactionModel
       */
        /// <summary>
        /// Get checks list for review
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getChecksForReview")]
        public IEnumerable<TransactionModel> GetChecksForReview(string id)
        {
            ActivityLogHelper activityLogHelper = new ActivityLogHelper();
            CheckService checkService = new CheckService(ConnectorServicesFactory.GetChecksInstance());
            var _documentDbService = DocumentDbServicesFactory<TransactionModel>.GetDocumentDbInstance();

            //Prepared to RequestModel object to provide the details while logging the error.
            RequestModel request = activityLogHelper.PrepareRequestModel(Request.Headers["User-Agent"].ToString(), Request.Headers["X-Original-For"].ToString(), Request.Headers["Host"].ToString(), "Get", "Check", "Gets check for review");

            IEnumerable<TransactionModel> transactionList = checkService.GetChecksForReview(_documentDbService, id, VCSConstants.Transaction, request);
            return transactionList;
        }

        /*
        * Function deletes the check details from DB based on parameters passed.
        *
        * param transaction model instance
        *
        * name   DeleteCheckDetails
        * access public
        * author VCI <info@vericheck.net>
        *
        * return IEnumerable of TransactionModel
       */
        /// <summary>
        /// Delete check details
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        [HttpPost("deleteCheckDetails")]
        public async Task<bool> DeleteCheckDetails([FromBody]TransactionModel transaction)
        {
            ActivityLogHelper activityLogHelper = new ActivityLogHelper();
            //Prepared to RequestModel object to provide the details while logging the error.
            RequestModel request = activityLogHelper.PrepareRequestModel(Request.Headers["User-Agent"].ToString(), Request.Headers["X-Original-For"].ToString(), Request.Headers["Host"].ToString(), "Post", "Check", "Delete check from review page");
            bool status = false;
            try
            {
                if (transaction != null)
                {
                    CheckService checkService = new CheckService(ConnectorServicesFactory.GetChecksInstance());
                    var _documentDbService = DocumentDbServicesFactory<TransactionModel>.GetDocumentDbInstance();
                    status = await checkService.DeleteCheckDetails(_documentDbService, transaction.Id, request);
                   
                    if (status)
                    {
                        BlobsHelper blobHelper = new BlobsHelper(transaction.Merchant_Id, request);
                        await blobHelper.DeleteFileFromBlob(transaction.Front_Image_Name, request);
                        status = await blobHelper.DeleteFileFromBlob(transaction.Back_Image_Name, request);
                    }
                }
            }
            catch(Exception ex)
            {
                ServiceBusConnector serviceBus = new ServiceBusConnector();
                serviceBus.LogError(ex, request);
                return false;
            }
            return status;
        }

        /*
        * Function updates the check details in the DB based on parameters passed to show for review purpose.
        *
        * param merchant Id
        *
        * name   UpdateChecksForReview
        * access public
        * author VCI <info@vericheck.net>
        *
        * return string
       */
        /// <summary>
        /// Update/delete check details before review
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        [HttpGet("updateChecksForReview")]
        public async Task<string> UpdateChecksForReview(string merchantId)
        {
            string status = string.Empty;
            if (!string.IsNullOrEmpty(merchantId))
            {
                ActivityLogHelper activityLogHelper = new ActivityLogHelper();
                CheckService checkService = new CheckService(ConnectorServicesFactory.GetChecksInstance());
                var _documentDbService = DocumentDbServicesFactory<TransactionModel>.GetDocumentDbInstance();
                //Prepared to RequestModel object to provide the details while logging the error.
                RequestModel request = activityLogHelper.PrepareRequestModel(Request.Headers["User-Agent"].ToString(), Request.Headers["X-Original-For"].ToString(), Request.Headers["Host"].ToString(), "Get", "Check", "Updates check for review");

                IEnumerable<TransactionModel> transactionList = checkService.GetUnsavedChecks(_documentDbService, merchantId, VCSConstants.Transaction, request);
                if (transactionList != null && transactionList.Any())
                {
                    //Delete images from blob storage
                    BlobsHelper blobHelper = new BlobsHelper(transactionList.FirstOrDefault().Merchant_Id, request);
                    foreach (var transaction in transactionList)
                    {
                        if (!string.IsNullOrEmpty(transaction.Front_Image_Name) && !string.IsNullOrEmpty(transaction.Back_Image_Name))
                        {
                            await blobHelper.DeleteFileFromBlob(transaction.Front_Image_Name, request);
                            await blobHelper.DeleteFileFromBlob(transaction.Back_Image_Name, request);
                        }
                    }
                }
                status = await checkService.UpdateChecksForReview(_documentDbService, merchantId, VCSConstants.Transaction, request);
            }
            return status;
        }

        /*
          * Function gets the image model based on the paramaters passed.
          *
          * param imageModel
          *
          * name   GetImage
          * access public
          * author VCI <info@vericheck.net>
          *
          * return ImageModel
     */
        /// <summary>
        /// Get check images for review
        /// </summary>
        /// <param name="imageModel"></param>
        /// <returns></returns>
        [HttpPost("getCheckImages")]
        public async Task<ImageModel> GetImage([FromBody]ImageModel imageModel)
        {
            ActivityLogHelper activityLogHelper = new ActivityLogHelper();
            //Prepared to RequestModel object to provide the details while logging the error.
            RequestModel request = activityLogHelper.PrepareRequestModel(Request.Headers["User-Agent"].ToString(), Request.Headers["X-Original-For"].ToString(), Request.Headers["Host"].ToString(), "Get", "Check", "Updates check for review");
            try
            {
                if (imageModel != null)
                {
                    BlobsHelper blobHelper = new BlobsHelper(imageModel.Merchant_Id, request);
                    return await blobHelper.GetSingleImageContent(imageModel, request);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ServiceBusConnector serviceBus = new ServiceBusConnector();
                serviceBus.LogError(ex, request);
                return null;
            }
        }
    }
}
