﻿/*
 * VERICHECK INC CONFIDENTIAL
 * Vericheck Incorporated
 * All Rights Reserved.
 
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * category Controller
 * package  ReportController
 * author   VCI <info@vericheck.net>
 * license  Copyright 2018 VeriCheck | All Rights Reserved
 * version  GIT:$Id:
 * link     https://www.vericheck.com/doc
 */

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Phoenix.CheckScanner.Connectors;
using Phoenix.CheckScanner.Connectors.Azure;
using Phoenix.CheckScanner.Connectors.Azure.DocumentDB;
using Phoenix.CheckScanner.Connectors.Contracts;
using Phoenix.CheckScanner.UI.Services.Azure;
using Phoenix.CheckScanner.UI.Services.Helpers;
using Phoenix.CheckScanner.UI.Services.Services;
using System.Collections.Generic;

namespace Phoenix.CheckScanner.UI.Services.Controllers
{
    /*
    * Report Controller used to get the transaction reports of merchants 
    *
    * name     ReportController
    * category Controller
    * package  Report Controller
    * author   VCI <info@vericheck.net>
    * license  Copyright 2018 VeriCheck | All Rights Reserved
    * version  GIT:
    * link     https://www.vericheck.com/docs/
    */
    [Produces("application/json")]
    [Route("reports")]
    [EnableCors("ScannerCors")]
    [AuthorizeToken]
    public class ReportController : Controller
    {

        /*
        * Function gets the transaction report based on the parameters passed.
        *
        * param Obj documentDBService is used to access the services of DocumentDB, reportFilters contains all the properties based on which the Transaction  report is generated,
        * Request object is used during activity and error logging.
        *
        * name   GetTransactionReport
        * access public
        * author VCI <info@vericheck.net>
        *
        * return TransactionModel 
   */
        [HttpPost("getTransactionReport")]
        public IEnumerable<TransactionModel> GetTransactionReport([FromBody]ReportFilters reportFilters)
        {
            IEnumerable<TransactionModel> transactionList = null;
            if (reportFilters != null)
            {
                ActivityLogHelper activityLogHelper = new ActivityLogHelper();
                ReportService reportService = new ReportService(ConnectorServicesFactory.GetReportsInstance());
                var _documentDbService = DocumentDbServicesFactory<TransactionModel>.GetDocumentDbInstance();

                //Prepared to RequestModel object to provide the details while logging the error.
                RequestModel request = activityLogHelper.PrepareRequestModel(Request.Headers["User-Agent"].ToString(), Request.Headers["X-Original-For"].ToString(), Request.Headers["Host"].ToString(), "GetReport", "Reports", "Get transaction reports");
                transactionList = reportService.GetTransactionReport(_documentDbService, reportFilters, request);

            }
            return transactionList;
        }
    }
}