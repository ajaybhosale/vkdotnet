﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using PhnxScanner.Cache.Redis;
using Phoenix.CheckScanner.Connectors;
using Phoenix.CheckScanner.Connectors.Azure.ServiceBus;
using Phoenix.CheckScanner.Connectors.Contracts;
using Phoenix.CheckScanner.UI.Services.Auth;
using Phoenix.CheckScanner.UI.Services.Helpers;
using System;
using System.Threading.Tasks;

namespace Phoenix.CheckScanner.UI.Services
{
    /*
     * Authentication Controller works as a point of contact to receive the login, Logout, forgot password requests.
     *
     * name     AuthenticationController
     * category Controller
     * package  Authentication Controller
     * author   VCI <info@vericheck.net>
     * license  Copyright 2018 VeriCheck | All Rights Reserved
     * version  GIT:
     * link     https://www.vericheck.com/docs/
     */
    [Produces("application/json")]
    [Route("api/authentication")]
    [EnableCors("ScannerCors")]
    public class AuthenticationController : Controller
    {
        private readonly KeyVaultSettings _keyVaultSettings;
        private readonly IKeyVaultSecret _keyVaultSecret;
        public RedisCache<KeyVaultModel> _cache { get; set; }
        private readonly AzureCredentialsSettings _keyvaultModel;
        public AuthenticationController(IKeyVaultSecret keyVaultSecret,
            IOptions<KeyVaultSettings> keyVaultSettings,IOptions<AzureCredentialsSettings> keyvaultModel)
        {
            _keyVaultSecret = keyVaultSecret;
            _keyVaultSettings = keyVaultSettings.Value;
            _cache = new RedisCache<KeyVaultModel>();
            _keyvaultModel = keyvaultModel.Value;
        }

        /*
         * Function authenticates the logged in user based the code parameter passed.
         *
         * param code
         *
         * name   Login
         * access public
         * author VCI <info@vericheck.net>
         *
         * return string
    */
        /// <summary>
        /// User authentication
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet("login")]
        public async Task<string> Login(string code) // get the user obj from Anguler
        {
            string keyvaultSettings = string.Empty;
            string accessTokenObject = string.Empty;
            ActivityLogHelper activityLogHelper = new ActivityLogHelper();
            activityLogHelper.LogActivity(Request, "Get", "Authentication", "Login operation");
            //Prepared to RequestModel object to provide the details while logging the error.
            RequestModel request = activityLogHelper.PrepareRequestModel(Request.Headers["User-Agent"].ToString(), Request.Headers["X-Original-For"].ToString(), Request.Headers["Host"].ToString(), "Get", "Authentication", "Login operation");

            try
            {
                //Get DB credentials from KeyVault
                //if (_keyVaultSettings.Environment == VCSConstants.DIT)
                //{
                //    keyvaultSettings = await _keyVaultSecret.GetSecretAsync("scanner-dit", request);
                //}
                //else if (_keyVaultSettings.Environment == VCSConstants.SIT)
                //{
                //    keyvaultSettings = await _keyVaultSecret.GetSecretAsync("phnx-scanner-common-sit", request);
                //}
                //else if (_keyVaultSettings.Environment == VCSConstants.UAT)
                //{
                //    keyvaultSettings = await _keyVaultSecret.GetSecretAsync("phnx-scanner-common-uat", request);
                //}
                //else if (_keyVaultSettings.Environment == VCSConstants.Production)
                //{
                //    keyvaultSettings = await _keyVaultSecret.GetSecretAsync("phnx-scanner-common-production", request);
                //}
                KeyVaultModel keyvaultModel= new KeyVaultModel() {
                    AzureCredentialsSettings=_keyvaultModel
                };// = JsonConvert.DeserializeObject<KeyVaultModel>(keyvaultSettings);
                //if (keyvaultModel != null)
                //{
                //    //Insert DB credentials in cache and add expiration
                //    _cache.Put("scannerCache", keyvaultModel);
                //    _cache.Expire("scannerCache", DateTimeOffset.Now.AddHours(_keyVaultSettings.Timeout));
                //}
                AuthenticationService authenticationService = new AuthenticationService(ConnectorServicesFactory.GetActiveDirectoryInstance());
                keyvaultModel.AzureCredentialsSettings.Code = code;
                accessTokenObject = authenticationService.ValidateAzureCredentials(keyvaultModel.AzureCredentialsSettings);
                if (accessTokenObject != null && accessTokenObject != "Error") {
                   
                }
                else
                {
                    _cache.Clear("scannerCache");
                }
                
            }
            catch (Exception ex)
            {
                ServiceBusConnector serviceBus = new ServiceBusConnector();
                serviceBus.LogError(ex, request);
                return "errorFromAzure";
            }
          
            return accessTokenObject;
        }
        [HttpGet("GetClientToken")]
        public async Task<string> GetClientToken()
        {
            ActivityLogHelper activityLogHelper = new ActivityLogHelper();
            RequestModel request = activityLogHelper.PrepareRequestModel(Request.Headers["User-Agent"].ToString(), Request.Headers["X-Original-For"].ToString(), Request.Headers["Host"].ToString(), "Get", "Authentication", "Login operation");

            var keyvaultSettings = await _keyVaultSecret.GetSecretAsync("scanner-dev", request);
            KeyVaultModel keyvaultModel = JsonConvert.DeserializeObject<KeyVaultModel>(keyvaultSettings);

            string accessTokenObject = string.Empty;
            AuthenticationService authenticationService = new AuthenticationService(ConnectorServicesFactory.GetActiveDirectoryInstance());
            accessTokenObject = await authenticationService.GetClientToken(keyvaultModel.AzureCredentialsSettings);
            return accessTokenObject;
        }

        [HttpGet("refreshtokens")]
        public string Refreshtokens()
        {
            return "Test..Success";
            //Prepared to RequestModel object to provide the details while logging the error.
            // RequestModel request = activityLogHelper.PrepareRequestModel(Request.Headers["User-Agent"].ToString(), Request.Headers["X-Original-For"].ToString(), Request.Headers["Host"].ToString(), "Post", "Transaction", "Making transactions");


        }

    }
}
