﻿/*
 * VERICHECK INC CONFIDENTIAL
 * Vericheck Incorporated
 * All Rights Reserved.
 
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * category Vericheck Constants
 * package  VCSConstants
 * author   VCI <info@vericheck.net>
 * license  Copyright 2018 VeriCheck | All Rights Reserved
 * version  GIT:$Id:
 * link     https://www.vericheck.com/doc
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.CheckScanner.UI.Services.Helpers
{
    public class VCSConstants
    {
        /*
       * Default constructor 
       *
       * name   VCSConstants
       * access public
       * author VCI <info@vericheck.net>
       *
       * return void
       */
        protected VCSConstants()
        {

        }
        #region Database Environments
        public const string DIT = "DIT";
        public const string SIT = "SIT";
        public const string UAT = "UAT";
        public const string Production = "Production";
        #endregion

        #region Collection Types
        public const string Scanner = "scanner";
        public const string Transaction = "transaction";
        #endregion
    }

    public class GatewayTypes
    {
        protected GatewayTypes() { }
        public const string VCI = "VCI";
        public const string USA = "USA";
        public const string PHN = "PHN";
    }
}
