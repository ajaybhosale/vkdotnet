﻿/*
 * VERICHECK INC CONFIDENTIAL
 * Vericheck Incorporated
 * All Rights Reserved.
 
 * NOTICE:
 * All information contained herein is, and remains the property of
 * Vericheck Inc, if any.  The intellectual and technical concepts
 * contained herein are proprietary to Vericheck Inc and may be covered
 * by U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Vericheck Inc.
 *
 * category Helper
 * package  BlobsHelper
 * author   VCI <info@vericheck.net>
 * license  Copyright 2018 VeriCheck | All Rights Reserved
 * version  GIT:$Id:
 * link     https://www.vericheck.com/doc
 */

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using PhnxScanner.Cache.Redis;
using Phoenix.CheckScanner.Connectors;
using Phoenix.CheckScanner.Connectors.Azure.ServiceBus;
using Phoenix.CheckScanner.Connectors.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.CheckScanner.UI.Services.Helpers
{
    public class BlobsHelper
    {
        readonly CloudBlobContainer container;
        public RedisCache<KeyVaultModel> _cache { get; set; }
        KeyVaultModel keyvaultSettings;
        /*
          * Parameterized constructor to iniliaze the Cloud storageAccount instance
          *
          * param merchantId and requestModel
          * 
          * name   BlobsHelper
          * access public
          * author VCI <info@vericheck.net>
          *
          * return void
          */
        /// <summary>
        /// initializes class properties  
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="requestModel"></param>
        public BlobsHelper(string merchantId, RequestModel requestModel = null)
        {
            _cache = new RedisCache<KeyVaultModel>();
           var settings = _cache.GetCacheItem("scannerCache");
            keyvaultSettings = settings.Value;
            CloudStorageAccount storageAccount = new CloudStorageAccount(
                        new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(
                          keyvaultSettings.BlobStorageSettings.AccountName,
                           keyvaultSettings.BlobStorageSettings.KeyValue), true);

            // Create a blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
           
            // Get a reference to a container named "scannerblob."
            container = blobClient.GetContainerReference(merchantId);
                      
            this.CreateBlobContainerIfNotExists(container, requestModel).Wait();

        }


        /*
      * Function uploads the image to blob datatype
      *
      * param list of transactions,
      * RequestModel object is used during activity and error logging.
      *
      * name   UploadImageToBlob
      * access public
      * author VCI <info@vericheck.net>
      *
      * return list of TransactionModel
     */
        /// <summary>
        /// Uploads image to Blob
        /// </summary>
        /// <param name="transactions"></param>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        public async Task<List<TransactionModel>> UploadImageToBlob(List<TransactionModel> transactions, RequestModel requestModel)
        {
            List<TransactionModel> updatedTransactions = new List<TransactionModel>();
            try
            {
                foreach (var transaction in transactions)
                {
                    if (!string.IsNullOrEmpty(transaction.Front_Image) && !string.IsNullOrEmpty(transaction.Back_Image))
                    {

                        transaction.Front_Image_Name = DateTime.Now.ToString("yyyyMMddHHmmssfff") + transaction.Check_Number + "_" + "F";
                        transaction.Back_Image_Name = DateTime.Now.ToString("yyyyMMddHHmmssfff") + transaction.Check_Number + "_" + "B";

                        CloudBlockBlob blockBlob = container.GetBlockBlobReference(transaction.Front_Image_Name);
                        blockBlob.Metadata.Add("MerchantId", transaction.Merchant_Id);
                        blockBlob.Metadata.Add("ContentType", "image");
                        blockBlob.Metadata.Add("Date", DateTime.UtcNow.ToString());
                        await blockBlob.UploadTextAsync(transaction.Front_Image);

                        blockBlob = container.GetBlockBlobReference(transaction.Back_Image_Name);
                        blockBlob.Metadata.Add("MerchantId", transaction.Merchant_Id);
                        blockBlob.Metadata.Add("ContentType", "image");
                        blockBlob.Metadata.Add("Date", DateTime.UtcNow.ToString());
                        await blockBlob.UploadTextAsync(transaction.Back_Image);
                    }
                    updatedTransactions.Add(transaction);
                }
            }
            catch (Exception ex)
            {
                ServiceBusConnector serviceBus = new ServiceBusConnector();
                serviceBus.LogError(ex, requestModel);
            }
            return updatedTransactions;
        }

        /*
        * Function gets the image by blob id.
        *
        * param IEnumerable of Transactions,
        * RequestModel object is used during activity and error logging.
        *
        * name   GetImageContentFromAzureByBlobId
        * access public
        * author VCI <info@vericheck.net>
        *
        * return list of TransactionModel
       */
        /// <summary>
        /// Gets Image content from Azure by blob Id.
        /// </summary>
        /// <param name="transactions"></param>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        public async Task<List<TransactionModel>> GetImageContentFromAzureByBlobId(IEnumerable<TransactionModel> transactions, RequestModel requestModel)
        {
            List<TransactionModel> updatedTransactions = new List<TransactionModel>();
            try
            {
                if (transactions != null && transactions.Any())
                {
                    foreach (var transaction in transactions)
                    {
                        if (!string.IsNullOrEmpty(transaction.Front_Image_Name) && !string.IsNullOrEmpty(transaction.Back_Image_Name))
                        {
                            CloudBlockBlob blockBlob = container.GetBlockBlobReference(transaction.Front_Image_Name);
                            transaction.Front_Image = await blockBlob.DownloadTextAsync();

                            blockBlob = container.GetBlockBlobReference(transaction.Back_Image_Name);
                            transaction.Back_Image = await blockBlob.DownloadTextAsync();

                        }
                        updatedTransactions.Add(transaction);
                    }
                }
            }
            catch (Exception ex)
            {
                ServiceBusConnector serviceBus = new ServiceBusConnector();
                serviceBus.LogError(ex, requestModel);
                return null;
            }
            return updatedTransactions;
        }


        /*
        * Function Gets single image content with help of ImageModel.
        *
        * param imagemodel,
        * RequestModel object is used during activity and error logging.
        *
        * name   GetImageContentFromAzureByBlobId
        * access public
        * author VCI <info@vericheck.net>
        *
        * return list of TransactionModel
       */
        /// <summary>
        /// Gets single image content with help of ImageModel.
        /// </summary>
        /// <param name="imageModel"></param>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        public async Task<ImageModel> GetSingleImageContent(ImageModel imageModel, RequestModel requestModel)
        {
            try
            {
                    if (imageModel != null && !string.IsNullOrEmpty(imageModel.Front_Image_Name) && !string.IsNullOrEmpty(imageModel.Back_Image_Name))
                    {
                        CloudBlockBlob blockBlob = container.GetBlockBlobReference(imageModel.Front_Image_Name);
                        imageModel.Front_Image = await blockBlob.DownloadTextAsync();

                        blockBlob = container.GetBlockBlobReference(imageModel.Back_Image_Name);
                        imageModel.Back_Image = await blockBlob.DownloadTextAsync();
                    }
                
            }
            catch (Exception ex)
            {
                ServiceBusConnector serviceBus = new ServiceBusConnector();
                serviceBus.LogError(ex, requestModel);
                return null;
            }
            return imageModel;
        }

        /*
        * Function Deletes file from Blob
        *
        * param filename ,
        * RequestModel object is used during activity and error logging.
        *
        * name   DeleteFileFromBlob
        * access public
        * author VCI <info@vericheck.net>
        *
        * return bool
       */
        /// <summary>
        /// Deletes file from Blob.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        public async Task<bool> DeleteFileFromBlob(string fileName, RequestModel requestModel)
        {
            try
            {
                if (!string.IsNullOrEmpty(fileName))
                {
                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);
                    await blockBlob.DeleteAsync();
                }
                return true;
            }
            catch (Exception ex)
            {
                ServiceBusConnector serviceBus = new ServiceBusConnector();
                serviceBus.LogError(ex, requestModel);
                return false;
            }
        }

        /*
          * Function Updates the metadata of transactions
          *
          * param list of transactions ,
          * RequestModel object is used during activity and error logging.
          *
          * name   UpdateMetaData
          * access public
          * author VCI <info@vericheck.net>
          *
          * return Task
         */
        /// <summary>
        /// Update metadata based on transactionList passed.
        /// </summary>
        /// <param name="transactionList"></param>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        public async Task UpdateMetaData(IEnumerable<TransactionModel> transactionList, RequestModel requestModel)
        {
            try
            {
                foreach (var transaction in transactionList)
                {
                    if (!string.IsNullOrEmpty(transaction.Trasaction_Id))
                    {
                        CloudBlockBlob frontImageBlob = container.GetBlockBlobReference(transaction.Front_Image_Name);
                        CloudBlockBlob backImageBlob = container.GetBlockBlobReference(transaction.Back_Image_Name);
                        //Front Image metadata update
                        await container.FetchAttributesAsync();
                        if (frontImageBlob.Metadata.ContainsKey("TransactionId"))
                        {
                            frontImageBlob.Metadata["TransactionId"] = transaction.Trasaction_Id;
                        }
                        else
                            frontImageBlob.Metadata.Add("TransactionId", transaction.Trasaction_Id);

                        await container.SetMetadataAsync();

                        //Back Image metadata update
                        if (backImageBlob.Metadata.ContainsKey("TransactionId"))
                        {
                            backImageBlob.Metadata["TransactionId"] = transaction.Trasaction_Id;
                        }
                        else
                            backImageBlob.Metadata.Add("TransactionId", transaction.Trasaction_Id);

                        await container.SetMetadataAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                ServiceBusConnector serviceBus = new ServiceBusConnector();
                serviceBus.LogError(ex, requestModel);
            }
        }

        /*
          * Function creates blob container if not exists
          *
          * param container ,
          * RequestModel object is used during activity and error logging.
          *
          * name   CreateBlobContainerIfNotExists
          * access public
          * author VCI <info@vericheck.net>
          *
          * return Task
         */
        /// <summary>
        /// Creates Blob container if not exists.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        private async Task CreateBlobContainerIfNotExists(CloudBlobContainer container, RequestModel requestModel)
        {
            try
            {
                // If "mycontainer" doesn't exist, create it.
                await container.CreateIfNotExistsAsync();

                await container.SetPermissionsAsync(new BlobContainerPermissions
                {
                    PublicAccess = BlobContainerPublicAccessType.Blob
                });
            }
            catch (Exception ex)
            {
                ServiceBusConnector serviceBus = new ServiceBusConnector();
                serviceBus.LogError(ex, requestModel);
            }
        }
    }
}
